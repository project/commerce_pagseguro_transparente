jQuery(document).ready(function(){
  loadEvents();

  jQuery("#edit-commerce-payment-payment-method-pagseguro-transparentecommerce-payment-pagseguro-transparente").ajaxComplete(function(event,request, settings) {
    loadEvents();
  });
});

var currentBrand = "visa";

var loadEvents = function() {
  // Inicia sem sessionId
  var hasSessionId = false;
  updateSessionId();
  installmentQuantityEvents();
  updateInstallments(currentBrand);

  // Create tokens on form updates.
  jQuery("#edit-commerce-payment-payment-details-credit-card-number").change(function() {
    var cardBin = jQuery("#edit-commerce-payment-payment-details-credit-card-number").val();
    updateCardBrand(cardBin);
    updateTokens();
  });
  jQuery("#edit-commerce-payment-payment-details-credit-card-code").change(function() {
    updateTokens()
  });
  jQuery("#edit-commerce-payment-payment-details-credit-card-exp-month").change(function() {
    updateTokens()
  });
  jQuery("#edit-commerce-payment-payment-details-credit-card-exp-year").change(function() {
    updateTokens()
  });
};

var updateTokens = function() {
  if (
    jQuery("#edit-commerce-payment-payment-details-credit-card-number").val()
    && jQuery("#edit-commerce-payment-payment-details-credit-card-code").val()
  ) {
    updateSenderHash();
    updateCardToken();
  }
};

var updateSenderHash = function() {
  var senderHash = PagSeguroDirectPayment.getSenderHash();
  jQuery("[name='commerce_payment[payment_details][sender_hash]']").val(senderHash);
};

var updateSessionId = function(callback) {
  PagSeguroDirectPayment.setSessionId(Drupal.settings.pct.si);
};

// Atualiza dados de parcelamento atráves da bandeira do cartão
var updateInstallments = function(brand) {

  var amount = Number(jQuery("[name='commerce_payment[payment_details][order_total]']").val() / 100);
  var noInsterest = 6;

  PagSeguroDirectPayment.getInstallments({
    amount: amount,
    brand:  brand,
    maxInstallmentNoInterest: noInsterest,
    success: function(response) {

      // Para obter o array de parcelamento use a bandeira como "chave" da lista "installments"
      var installments = response.installments[brand];

      var options = ('<option value="0" dataPrice="0">-- Selecione --</option>');
      for (var i in installments) {

        var optionItem     = installments[i];
        var optionQuantity = optionItem.quantity; // Obtendo a quantidade
        var optionAmount   = optionItem.installmentAmount; // Obtendo o valor
        var optionLabel    = (optionQuantity + "x de " + formatMoney(optionAmount)); // montando o label do option

        if (i == 0) {
          var optionLabel = formatMoney(optionAmount);
          optionLabel += " à vista";
        }

        if (i > 0 && i < noInsterest) {
          optionLabel += " sem juros";
        }
        var price          = Number(optionAmount).toMoney(2,'.',',');

        options += ('<option value="' + optionItem.quantity + '" dataPrice="'+price+'">'+ optionLabel +'</option>');

      };
      //console.log(options);
      // Atualizando dados do select de parcelamento
      jQuery("#edit-commerce-payment-payment-details-installments").html(options);

      // Exibindo select do parcelamento
      //$("#installmentsWrapper").show();

      // Utilizando evento "change" como gatilho para atualizar o valor do parcelamento
      jQuery("#edit-commerce-payment-payment-details-installments").trigger('change');

    },
    error: function(response) {
      console.log(response);
    },
    complete: function(response) {

    }
  });

};

var updateCardBrand = function(cardBin) {

    PagSeguroDirectPayment.getBrand({

      cardBin: cardBin,

      success: function(response) {

        var brand = response.brand.name;

        if (currentBrand != brand) {
          currentBrand = brand;
          updateInstallments(brand);
        }

      },

      error: function(response) {

      },

      complete: function(response) {

      }

    });

  };

var updateCardToken = function() {

  PagSeguroDirectPayment.createCardToken({

    cardNumber: jQuery("#edit-commerce-payment-payment-details-credit-card-number").val(),
    brand: currentBrand,
    cvv: jQuery("#edit-commerce-payment-payment-details-credit-card-code").val(),
    expirationMonth: jQuery("#edit-commerce-payment-payment-details-credit-card-exp-month").val(),
    expirationYear: jQuery("#edit-commerce-payment-payment-details-credit-card-exp-year").val(),

    success: function(response) {

      // Obtendo token para pagamento com cartão
      var token = response.card.token;
      jQuery("[name='commerce_payment[payment_details][card_token]']").val(token);
      // Executando o callback (pagamento) passando o token como parâmetro
    },

    error: function(response) {

      showCardTokenErrors(response.errors);

    },

    complete: function(response) {

    }

  });
};

// Atualizando o valor do parcelamento
var installmentQuantityEvents = function() {
  jQuery("#edit-commerce-payment-payment-details-installments").change(function() {
    updateTokens();
    var option = jQuery(this).find("option:selected");
    if (option.length) {
      jQuery("[name='commerce_payment[payment_details][installments_value]']").val( option.attr("dataPrice") );
    }
  });
};

// Shipping specific stuff
var shippingEvents = function() {
  jQuery("[name='commerce_shipping[shipping_service]']").change(function() {
    var option = jQuery(this).find("checked:checked");
    if (option.length) {
      jQuery("[name='commerce_shipping[shipping_service]").val( option.attr("dataPrice") );
    }
  });
};

