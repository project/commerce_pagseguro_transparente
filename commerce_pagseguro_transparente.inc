<?php
/**
 * Payment method callback: settings form.
 */
function pagseguro_transparente_settings_form($settings = NULL) {
  $settings = (array) $settings + array(
    'email' => '',
    'token_pagseguro' => '',
    'individual_items' => FALSE,
    'log_debug_info' => '',
  );

  $form = array();
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#description' => t('The email address used for the PagSeguro account you want to receive payments.'),
    '#default_value' => $settings['email'],
    '#required' => TRUE,
  );

  $form['token_pagseguro'] = array(
    '#type' => 'textfield',
    '#title' => t('PagSeguro Token'),
    '#description' => t('The access token generated to your PagSeguro account. Required to process the "Automatic Data Return"'),
    '#default_value' => $settings['token_pagseguro'],
    '#size' => 50,
    '#maxlength' => 50,
    '#required' => FALSE,
  );

  $form['individual_items'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send individual items'),
    '#description' => t('Whether to send individual line items. If unchecked only one item will be sent summarizing the entire order.'),
    '#default_value' => $settings['individual_items'],
  );

  $form['log_debug_info'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log debug information'),
    '#description' => t('Log info about notifications.'),
    '#default_value' => $settings['log_debug_info'],
  );

  return $form;
}
/**
 * Payment method callback: submit form.
 */
function pagseguro_transparente_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $pagseguro_path = libraries_get_path('pagseguro');

  $fields = array(
    'owner' => '',
    //'type' => '',
    'number' => '',
    'start_month' => '',
    'code' => '',
  );
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  // Default to a known test credit card number. For valid numbers of other card
  // types see: http://www.rimmkaufman.com/blog/credit-card-test-numbers/09112007/
  $form = commerce_payment_credit_card_form($fields, array('number' => '4111111111111111'));

  $order_total = $order->commerce_order_total['und'][0]['amount'];
  $form['order_total'] = array('#type' => 'hidden', '#default_value' => $order_total);
  $form['card_token'] = array('#type' => 'hidden', '#default_value' => '');
  $form['sender_hash'] = array('#type' => 'hidden', '#default_value' => '');

  $form['dob'] = array(
    '#type' => 'date',
    '#title' => t('Data de nascimento'),
    '#required' => TRUE
  );

  $form['installments_value'] = array('#type' => 'hidden', '#default_value' => '');
  $form['installments'] = array(
       '#type' => 'select',
       '#required' => TRUE,
       '#title' => t('Em quantas parcelas deseja pagar?'),
       '#options' => array(
         0 => t('-- Selecione --'),
       ),
       '#validated' => TRUE
   );

  return $form;
}

/**
 * Payment method callback: submit form validation.
 */
function pagseguro_transparente_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  // Validate the credit card fields.
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  $settings = array(
    'form_parents' => array_merge($form_parents, array('credit_card')),
  );

  // Validate installments field.
  if ($pane_values['installments'] == 0) {
    form_set_error('installments', t('Por favor, selecione em quantas parcelas deseja pagar.'));
    return FALSE;
  }

  // Even though a form error triggered by the validate handler would be enough
  // to stop the submission of the form, it's not enough to stop it from a
  // Commerce standpoint because of the combined validation / submission going
  // on per-pane in the checkout form. Thus even with a call to form_set_error()
  // this validate handler must still return FALSE.
  if (!commerce_payment_credit_card_validate($pane_values['credit_card'], $settings)) {
    return FALSE;
  }

  if(!pagseguro_transparente_payment_credit_card($payment_method, $pane_form, $pane_values, $order, $form_parents = array())) {
    form_set_error("credit_card", "Houveram problemas ao processar seu cartão, por favor tente novamente ou entre em contato conosco.");
    return FALSE;
  }
}

/**
 * Payment method callback: submit form submission.
 */
function pagseguro_transparente_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  // Just as an example, we might store information in the order object from the
  // payment parameters, though we would never save a full credit card number,
  // even in examples!
  $number = $pane_values['credit_card']['number'];
  $pane_values['credit_card']['number'] = substr($number, 0, 4) . str_repeat('-', strlen($number) - 8) . substr($number, -4);

  $order->data['commerce_payment_example'] = $pane_values;

  // Every attempted transaction should result in a new transaction entity being
  // created for the order to log either the success or the failure.
  commerce_payment_example_transaction($payment_method, $order, $charge);
}

/**
 * Creates an example payment transaction for the specified charge amount.
 *
 * @param $payment_method
 *   The payment method instance object used to charge this payment.
 * @param $order
 *   The order object the payment applies to.
 * @param $charge
 *   An array indicating the amount and currency code to charge.
 */
function pagseguro_transparente_transaction($payment_method, $order, $charge) {
  $card_details = $order->data['commerce_payment_example']['credit_card'];

  $transaction = commerce_payment_transaction_new('commerce_pagseguro_transparente', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;

  $transaction->message = 'Number: @number<br/>Expiration: @month/@year';
  $transaction->message_variables = array(
    '@number' => $card_details['number'],
    '@month' => $card_details['exp_month'],
    '@year' => $card_details['exp_year'],
  );

  commerce_payment_transaction_save($transaction);
  return $transaction;
}

function pagseguro_transparente_payment_credit_card($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {

  $orderid = $order->order_id;
  $settings = $payment_method['settings'];

  // Return an error if the enabling action's settings haven't been configured.
  if (empty($settings['token_pagseguro']) || empty($settings['email'])) {
    drupal_set_message(t('Pagseguro Integration is not configured for use. You can ')
        . l(t('access PagSeguro Payment Method configuration'), 'admin/commerce/config/payment-methods/manage/commerce_payment_pagseguro')
        . t(' to do it now.')
        , 'error');
    return array();
  }

  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  commerce_discount_commerce_cart_order_refresh($order_wrapper);

  $hidden_fields = array(
    'receiverEmail' => $settings['email'],
    // Only Brazilian Real is allowed.
    'currency' => 'BRL',
    // Tell PagSeguro servers our post is going as utf-8 (Drupal's default encoding).
    'encoding' => 'utf-8',
    // Use the timestamp to generate a unique transaction number.
    'reference' => $order->order_number . '-' . REQUEST_TIME,
  );

  if ($order_wrapper->commerce_customer_shipping->value()) {
    $customer_shipping_address = $order_wrapper->commerce_customer_shipping->commerce_customer_address->value();
    $customer_shipping_address['cpf'] = $order_wrapper->commerce_customer_shipping->field_cpf->value();
    $customer_shipping_address['number'] = $order_wrapper->commerce_customer_shipping->field_number->value();
    $customer_shipping_address['telephone'] = $order_wrapper->commerce_customer_shipping->field_telefone->value();
  }

  // Set billing address info.
  $billing_info = $_POST['customer_profile_billing'];
  $customer_billing_address = array();
  if (isset($billing_info['commerce_customer_profile_copy'])) {
    $customer_billing_address = $customer_shipping_address;
  }
  else {
    $customer_billing_address += $billing_info['commerce_customer_address']['und'][0];
    $customer_billing_address['cpf'] = $billing_info['field_cpf']['und'][0]['value'];
    $customer_billing_address['number'] = $billing_info['field_number']['und'][0]['value'];
  }

  if (!empty($customer_shipping_address)) {

    if (empty($customer_shipping_address['name_line'])) {
      $customer_name = $customer_shipping_address['first_name'] . ' ' . $customer_shipping_address['last_name'];
    }
    else {
      $customer_name = $customer_shipping_address['name_line'];
    }

    /**
     * See PagSeguro docs on https://pagseguro.uol.com.br/v2/guia-de-integracao/pagamento-via-html.html
     */
    $customer_data = array(
      'senderName' => $customer_name,
      'senderEmail' => $order->mail,
      'shippingAddressPostalCode' => $customer_shipping_address['postal_code'],
    );
  }

  $hidden_fields = array_merge($hidden_fields, $customer_data);
  $hidden_fields['shipping'] = $customer_shipping_address;
  $hidden_fields['billing'] = $customer_billing_address;

  foreach ($order->commerce_line_items[LANGUAGE_NONE] as $key => $value) {
    $line_item_ids[] = $value['line_item_id'];
  }

  $line_items = commerce_line_item_load_multiple($line_item_ids);

  $hidden_fields['items'] = array();

  if ($settings['individual_items']) {
    $index = 0;
    foreach ($line_items as $line_item) {
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
      if ($line_item->type == 'product') {
        $line_item_id = $line_item_wrapper->commerce_product->product_id->value();
        $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);
        $hidden_fields['items'][$index] = array(
          'itemId' => $line_item_id,
          'itemDescription' => commerce_line_item_title($line_item),
          'itemQuantity' => round($line_item->quantity),
          // PagSeguro uses a number format different than Drupal, that's why the
          // amount is divided by 100
          'itemAmount' => number_format(($unit_price['amount'] / 100), 2),
        );
        $index++;
      }
      elseif ($line_item->type == 'commerce_discount') {
        $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);
        $hidden_fields['discount'] = number_format(($unit_price['amount'] / 100), 2);
      }
      elseif ($line_item->type == 'shipping') {
        $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);
        $hidden_fields['shipping_cost'] = number_format(($unit_price['amount'] / 100), 2);
      }
    }
    unset($index);
  }
  else {
    // Send everything as a single item.
    $order_total = $order_wrapper->commerce_order_total->amount->value();
    $hidden_fields = array(
      'itemId1' => $order->order_number,
      'itemDescription1' => t('Order @order_id', array('@order_id' => $order->order_number)),
      'itemQuantity1' => 1,
      'itemAmount1' => $order_total,
        ) + $hidden_fields;
  }
  $hidden_fields += $pane_values;

  return pagseguro_transparente_api_payment($order, $hidden_fields);
}

function pagseguro_transparente_api_payment($order, $hidden_fields) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $pagseguro_path = libraries_get_path('pagseguro');
  include_once($pagseguro_path . "/PagSeguroLibrary.php");

  // Instantiate a new payment request
  $paymentRequest = new PagSeguroDirectPaymentRequest();
  $paymentRequest->setPaymentMode('DEFAULT'); // GATEWAY
  $paymentRequest->setPaymentMethod('CREDIT_CARD');
  $paymentRequest->addParameter('noInterestInstallmentQuantity', 6);
  // Add installment without addition per payment method
  //$paymentRequest->addPaymentMethodConfig('CREDIT_CARD', 6, 'MAX_INSTALLMENTS_NO_INTEREST');

  // Set the currency
  $paymentRequest->setCurrency($hidden_fields['currency']);

  if (!empty($hidden_fields['discount'])){
    $paymentRequest->setExtraAmount($hidden_fields['discount']);
  }

  foreach($hidden_fields['items'] as $item) {
      $paymentRequest->addItem($item['itemId'] , $item['itemDescription'], $item['itemQuantity'], $item['itemAmount']);
  }
  // Set a reference code for this payment request. It is useful to identify this payment
  // in future notifications.
  $paymentRequest->setReference($hidden_fields['reference']);

  // Set shipping information for this payment request
  $sedexCode = PagSeguroShippingType::getCodeByType('SEDEX');
  $paymentRequest->setShippingType($sedexCode);
  $paymentRequest->setShippingCost($hidden_fields['shipping_cost']);

  $paymentRequest->setShippingAddress(
      $hidden_fields['shipping']['postal_code'],
      $hidden_fields['shipping']['thoroughfare'],
      $hidden_fields['shipping']['number'],
      $hidden_fields['shipping']['premise'],
      $hidden_fields['shipping']['dependent_locality'],
      $hidden_fields['shipping']['locality'],
      $hidden_fields['shipping']['administrative_area'],
      $hidden_fields['shipping']['country']
  );

  // Set your customer information.
  /*$paymentRequest->setSender(
      $hidden_fields['senderName'],
      "testvin@sandbox.pagseguro.com.br",//$hidden_fields['senderEmail'],
      '11',
      '56273440',
      'CPF',
      '156.009.442-76'
  );*/
  $phone_field = explode(' ', $hidden_fields['shipping']['telephone']);
  $area_code = $phone_field[0];
  $phone_number = str_replace("-", "", $phone_field[1]);
  // Set your customer information.
  $paymentRequest->setSender(
      $hidden_fields['senderName'],
      $hidden_fields['senderEmail'],//$hidden_fields['senderEmail'],
      $area_code,
      $phone_number,
      'CPF',
      $hidden_fields['shipping']['cpf']
  );

  $paymentRequest->setSenderHash($hidden_fields['sender_hash']);

  // Credit Card info.
  $installments = new PagSeguroInstallment(
    array(
      'quantity' => $hidden_fields['installments'],
      'value' => $hidden_fields['installments_value']
    )
  );

  $creditCardToken = $hidden_fields['card_token'];

  $billing = array(
    'postalCode' => $hidden_fields['billing']['postal_code'],
    'street' => $hidden_fields['billing']['thoroughfare'],
    'number' =>  $hidden_fields['billing']['number'],
    'complement' => $hidden_fields['billing']['premise'],
    'district' => $hidden_fields['billing']['dependent_locality'],
    'city' => $hidden_fields['billing']['locality'],
    'state' => $hidden_fields['billing']['administrative_area'],
    'country' => $hidden_fields['billing']['country'],
  );

  $billingAddress = new PagSeguroBilling($billing);

  // Get billing address phone values.
  $phone_field = explode(' ', $hidden_fields['shipping']['telephone']);
  $area_code = $phone_field[0];
  $phone_number = str_replace("-", "", $phone_field[1]);
  $dob_day = sprintf('%02d', $hidden_fields['dob']['day']);
  $dob_month = sprintf('%02d', $hidden_fields['dob']['month']);
  $birthdate = $dob_day . '/' . $dob_month . '/' . $hidden_fields['dob']['year'];

  $creditCardData = new PagSeguroCreditCardCheckout(
  array(
    'token' => $creditCardToken,
    'installment' => $installments,
    'billing' => $billingAddress,
    'holder' => new PagSeguroCreditCardHolder(
      array(
        'name' => $hidden_fields['billing']['name_line'],
        'birthDate' => date($birthdate),
        'areaCode' => $area_code,
        'number' => $phone_number,
        'documents' => array(
          'type' => 'CPF',
          'value' => $hidden_fields['billing']['cpf'],
        )
      )
    )
  )
);

$paymentRequest->setCreditCard($creditCardData);


  try {

      /*
       * #### Credentials #####
       * Replace the parameters below with your credentials
       * You can also get your credentials from a config file. See an example:
       * $credentials = new PagSeguroAccountCredentials("vendedor@lojamodelo.com.br",
       * "E231B2C9BCC8474DA2E260B6C8CF60D3");
       */

      // seller authentication
      $credentials = PagSeguroConfig::getAccountCredentials();

      $response = $paymentRequest->register($credentials);
      return TRUE;

  } catch (PagSeguroServiceException $e) {
      watchdog('pagseguro_transparente', $e->getMessage());
      form_set_error('pagseguro_transparente', $e->getMessage());
      return FALSE;
  }
}


